
---

# Overview

---

## What is  TaskFile ?

- Better alternative to GNU Makefiles, it uses YAML format
- Centralize place for commands that run in local and CI/CD
- Internally  powered by [mvdan/sh](https://github.com/mvdan/sh) and  a slim  fork  of  [Masterminds/sprig](https://github.com/Masterminds/sprig)
- Supports bash