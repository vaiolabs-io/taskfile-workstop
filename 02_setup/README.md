
---

# Setup

---

# Setup

## How to install 

- Prebuilt binaries 
  - Can be installed with shell script
  - Brew/Snap/Yay/Dnf installation exist, but version differences may vary
- Source installation 
 - In case `Go` development environment is install, can be installed as a module

