
---

# Quick Start

---

# Quick Start

## 

- Can set tasks declaratively in `Taskfile.yml` or `Taskfile.yaml` file
- Can run multiple tasks at once as well as just once at a time
- Can also set default task which runs when `task ls` typed`
- All short descriptions are available via `task -l`
- Silent options is preferred for not outputting written script

---

# Example

```yaml
version: '3'
silent: true
tasks:
    default:
        desc: "prints hello world"
        cmds:
            - echo "Hello World"

    goodbye:
        desc: "prints GoodBye World"
        cmds:
            - echo "GoodBye World"
        
    test:
        desc: "prints Test Runs Here"
        cmds: 
            - sleep 2
            - echo "Test Runs Here"
```

---

# Execution

```sh
$ task
Hello World
$ task goodbye
GoodBye World
$ task goodbye test
GoodBye World 
Test Runs Here
```

---

# Practice

