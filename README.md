# Taskfile Workshop

Task is a task runner / build tool that aims to be simpler and easier to use than, GNU Make

- [Intro](./00_intro/README.md)
- [Task_file](./00_intro/README.md)
- [Setup](./02_setup/README.md)
- [Quick Start](./03_quick_start/README.md)
- [Environment Variables](./04_env_vars/README.md)
- [Dynamic Variables](./05_dyn_vars/README.md)
- [Go Templates](./06_go_templates/README.md)
- [Parallel Execution](./07_parallel_execution/README.md)
- [Namespaces](./08_namespaces/README.md)
- [Includes](./09_includes/README.md)

<!-- based on youtube tutorial https://www.youtube.com/watch?v=J6U9M1alnsE -->
